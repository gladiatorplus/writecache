// xhEasyWriter.cpp: implementation of the xhEasyWriter class.
//
//////////////////////////////////////////////////////////////////////

#include "xhEasyWriter.h"
#include<iostream>
#include<cstring>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

xhEasyWriter::xhEasyWriter()
{
	m_hFile = NULL;

	m_llFileSize = 0;
	m_llSetPos = 0;
	m_llRealPos = 0;
	m_pBuf = NULL;
	m_nBufLen = 0;
	m_llBufBeginPos = 0;
	m_nBufValidData = 0;
	//m_nWriteBlockSize = 2 * 1024 * 1024;
	m_nWriteBlockSize = 0;
	m_nReWriteBlockSize = 4096;
	m_nPageSize = 4096;
	m_nPosPart = 0;
	m_pTempReadBuf = NULL;
	m_nTempReadBufSize = 0;
	m_llFileSize = 0;
	m_nReadSize = 0;

	m_llMaxByteForFlush = 0;
}

xhEasyWriter::~xhEasyWriter()
{
}

int xhEasyWriter::Initialize()
{
	return 1;
}

int xhEasyWriter::Open(const char *pFileName, int nFlag /* = 0 */)
{
	int nRet = 1;

	// Close();

	std::string cMode;

	if(nFlag)
	{
		cMode = "r";
	}
	else
	{
		cMode = "w";
	}

	m_hFile = fopen(pFileName, cMode.c_str());

	if (NULL == m_hFile)
	{
		printf("xhEasyWriter::Open Failed,NULL == m_hFile\n");
		return -1;
	}

	m_llFileSize = GetFileSize();

	return 1;
}

long long xhEasyWriter::GetFileSize()
{
	int nRet = 1;

	if (NULL == m_hFile)
	{
		printf("xhEasyWriter::GetFileSize Failed,NULL == m_hFile\n");
		return 0;
	}

	return m_llFileSize;
}

int xhEasyWriter::SetPos(long long llPos, int nMoveMethod /* = FILE_BEGIN */)
{
	int nRet = 1;

	if (NULL == m_hFile)
	{
		printf("xhEasyWriter::SetPos Failed,NULL == m_hFile\n");
		return 0;
	}

	long long llAbsolutePos;

	switch (nMoveMethod)
	{
	case 0:
		llAbsolutePos = llPos;
		break;
	case 1:
		llAbsolutePos = m_llSetPos + llPos;
		break;
	case 2:
		llAbsolutePos = m_llFileSize + llPos;
		break;
	default:
		llAbsolutePos = llPos;
		break;
	}

	if (llAbsolutePos < 0)
	{
		printf("xhEasyWriter::SetPos Failed,llAbsolutePos < 0\n");
		return 0;
	}

	m_llSetPos = llAbsolutePos;

	return 1;
}

long long xhEasyWriter::GetPos()
{
	if (NULL == m_hFile)
	{
		printf("xhEasyWriter::GetPos Failed, NULL == m_hFile\n");
		return 0;
	}

	return m_llSetPos;
}

int xhEasyWriter::SetWriteBlockSize(int nWriteBlockSize)
{
	if (nWriteBlockSize <= 0)
	{
		printf("xhEasyWriter::SetWriteBlockSize Failed, nWriteBlockSize <= 0\n");
		return 0;
	}

	if (nWriteBlockSize > 1024 * 1024 * 8)
	{
		printf("xhEasyWriter::SetWriteBlockSize Failed,nWriteBlockSize > 1024 * 1024 * 8\n");
		return 0;
	}

	nWriteBlockSize = (nWriteBlockSize + m_nPageSize - 1) / m_nPageSize * m_nPageSize;

	m_nWriteBlockSize = nWriteBlockSize;

	return 1;
}

int xhEasyWriter::Write(char* pBuffer, int nLen)
{
	int nRet = 1;

	if (NULL == m_hFile)
	{
		printf("xhEasyWriter::Write Failed,NULL == m_hFile\n");
		return -1;
	}

	if (pBuffer == NULL || nLen < 0)
	{
		printf("xhEasyWriter::Write Failed,pBuffer == NULL || nLen < 0\n");
		return -1;
	}

	return WriteData(pBuffer, nLen);
}

int xhEasyWriter::WriteData(char* pBuffer, int nLen)
{
	char* pNowPos = pBuffer;
	int nNeedProcess = nLen;
	if(m_nBufValidData != 0)
	{
		//Buf中有数据
		if(m_llSetPos < m_llBufBeginPos)
		{
			RealWriteData();
		}
		else
		{
			if(m_llSetPos + nLen > m_llBufBeginPos + m_nBufValidData)
			{
				if(m_llSetPos <= m_llBufBeginPos + m_nBufValidData)
				{
					//数据有部分在缓冲中，需要拷贝前部分
					int nCopyOff = m_llSetPos - m_llBufBeginPos;
					int nCopyLen = m_nBufValidData - nCopyOff;
					if(nCopyLen > 0)
						memcpy(m_pBuf + nCopyOff,pNowPos,nCopyLen);
					nNeedProcess -= nCopyLen;
					pNowPos += nCopyLen;
				}
				else
				{
					//数据完全在缓冲后�?
					if(m_nReadSize > 0 && (m_llSetPos + nLen <= m_llBufBeginPos + m_nPageSize))
					{
						//为写头的小幅度跳写小额数据而优�?
						//要写的数据加上以前的数据一共没有page size�?
					 	memcpy(m_pBuf + (m_llSetPos - m_llBufBeginPos) , pBuffer , nLen);
						nNeedProcess -= nLen;
						m_nBufValidData = m_llSetPos - m_llBufBeginPos + nLen;
					}
					else
					{
						RealWriteData();
					}
				}
			}
			else
			{
				//要写数据完全在缓冲中
				int nCopyOff = m_llSetPos - m_llBufBeginPos;
				int nCopyLen = nLen;
				memcpy(m_pBuf + nCopyOff,pBuffer,nCopyLen);
				nNeedProcess = 0;
			}
		}
	}
	if(m_nBufValidData == 0)
	{
		SetRealPos(m_llSetPos);
		m_llBufBeginPos = m_llRealPos;
		if(m_nBufLen < m_nWriteBlockSize)
		{
			if(m_pBuf)
			{
				delete [] m_pBuf;
				m_pBuf = NULL;
			}
			m_pBuf = new char [m_nWriteBlockSize + m_nPageSize];
			m_nBufLen = m_nWriteBlockSize;
		}
	}

	while(nNeedProcess > 0)
	{
		//处理剩余数据
		if(m_nBufValidData >= m_nBufLen)
		{
			int nWriteLen = m_nBufValidData;
			RealWriteData();
			m_llBufBeginPos += nWriteLen;
			m_nBufValidData = 0;
		}
		int nProcessLen = m_nBufLen - m_nBufValidData;
		if(nProcessLen > nNeedProcess)
		{
			nProcessLen = nNeedProcess;
		}
		memcpy(m_pBuf + m_nBufValidData , pNowPos , nProcessLen);
		m_nBufValidData += nProcessLen;
		nNeedProcess -= nProcessLen;
		pNowPos += nProcessLen;
	}

	m_llSetPos += nLen;

	if(m_llFileSize < m_llSetPos)
	{
		m_llFileSize = m_llSetPos;
	}

	m_llMaxByteForFlush += nLen;

	if (m_llMaxByteForFlush > _MAX_BYTE_FOR_FLUSH_)
	{
		m_llMaxByteForFlush = 0;
		Flush();
	}

	return nLen;
}

int xhEasyWriter::Flush()
{
	int nRet = 1;

	if (m_hFile == NULL)
	{
		printf("xhEasyWriter::Flush Failed,m_hFile == NULL\n");
		return 0;
	}

	nRet = RealWriteData();

	if(nRet <= 0)
	{
		printf("xhEasyWriter::Flush, RealWriteData ret < 0\n");
		return 0;
	}

	nRet = fflush(m_hFile);

	if (nRet > 0)
	{
		return nRet;
	}
	else
	{
		printf("xhEasyWriter::Flush, fflush ret < 0\n");
		return -1;
	}
}

int xhEasyWriter::Close()
{
	int nRet = 1;

	nRet = RealWriteData();

	if (NULL == m_hFile)
	{
		printf("xhEasyWriter::Close Failed,NULL == m_hFile \n");
		return 0;
	}

	nRet &= fclose(m_hFile);

	m_hFile = NULL;

	m_llFileSize = 0;
	m_llSetPos = 0;
	m_llRealPos = 0;
	m_llBufBeginPos = 0;
	m_nBufValidData = 0;
	m_nBufLen = 0;
	m_nBufValidData = 0;
	m_nPosPart = 0;

	if (m_pBuf != NULL)
	{
		delete [] m_pBuf;
		m_pBuf = NULL;
	}

	if(m_pTempReadBuf)
	{
		delete [] m_pTempReadBuf;
		m_pTempReadBuf = NULL;
	}

	m_nTempReadBufSize = 0;
	m_nReadSize = 0;
	m_llBufBeginPos = 0;

	return nRet;
}

int xhEasyWriter::SetRealPos(long long llRealPos)
{
	if(m_llRealPos != llRealPos)
	{
		fpos_t liPos;
		liPos.__pos = llRealPos / m_nPageSize * m_nPageSize;

		int nRet = fsetpos(m_hFile, &liPos);

		if (nRet > 0)
		{
			m_llRealPos = liPos.__pos;

			m_nPosPart = 0;

			return nRet;
		}
	}
}

int xhEasyWriter::RealWriteData()
{
	int nRet = 1;

	if (NULL == m_hFile)
	{
		// printf("xhEasyWriter::RealWriteData Failed, NULL == m_hFile");
        return -1;
	}

	if(m_nBufValidData > 0)
	{
        m_nReadSize = 0;

		int nNeedWrite = m_nBufValidData;

		SetRealPos(m_llBufBeginPos);

		nRet = fwrite(m_pBuf, 1, nNeedWrite, m_hFile);

		if(nRet > 0)
		{
			m_nBufValidData = 0;

			m_llRealPos += nRet;
		}
	}

	return 1;
}

int xhEasyWriter::GetFileHandler() {
	if (m_hFile != NULL){
		return 1;
	}

	return -1;
}