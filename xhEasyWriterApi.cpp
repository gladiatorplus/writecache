#include "xhEasyWriter.h"
#include "xhEasyWriterApi.h"

void *Api_New() {
    return new xhEasyWriter();
}

int Api_Open(void* writer,const char *pFileName){
    xhEasyWriter* handler = (xhEasyWriter*)writer;
    return handler->Open(pFileName);
}

int Api_GetFileHandler(void* writer){
    xhEasyWriter* handler = (xhEasyWriter*)writer;
    return handler->GetFileHandler();
}

int Api_SetWriteBlockSize(void* writer,int nWriteBlockSize){
    xhEasyWriter* handler = (xhEasyWriter*)writer;
    return handler->SetWriteBlockSize(nWriteBlockSize);
}

int Api_Write(void* writer,char* pBuffer, int nLen){
    xhEasyWriter* handler = (xhEasyWriter*)writer;
    return handler->Write(pBuffer,nLen);
}

int Api_Close(void* writer){
    xhEasyWriter* handler = (xhEasyWriter*)writer;
    return handler->Close();   
}

void Api_Delete(void* writer) {
    xhEasyWriter* handler = (xhEasyWriter*)writer;
    if(handler != NULL){
        delete handler;
        handler = NULL;
    }
}
