// xhEasyWriter.h: interface for the xhEasyWriter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XHEASYWRITER_H__ED2AC049_1830_41B9_9159_35B1B877C8CD__INCLUDED_)
#define AFX_XHEASYWRITER_H__ED2AC049_1830_41B9_9159_35B1B877C8CD__INCLUDED_

#include <stdio.h>

#define OpenFileFailed       (0x00)
#define SetPosFailed         (0x01)
#define WriteDataFailed      (0x02)
#define GetFileSizeFailed    (0x03)
#define FlushOrCloseFailed   (0x04)

#define _MAX_BYTE_FOR_FLUSH_             16*1024*1024   //16M

class xhEasyWriter
{
public:
	xhEasyWriter();
	virtual ~xhEasyWriter();

	int Initialize();

	int Open(const char *pFileName, int nFlag = 0);

	long long GetFileSize();

	int SetPos(long long llPos, int nMoveMethod = 0);

	long long GetPos();

	int SetWriteBlockSize(int nWriteBlockSize);

	int Write(char* pBuffer, int nLen);

	int GetFileHandler();

	int Flush();

	int Close();

private:

	int WriteData(char* pBuffer, int nLen);

	int RealWriteData();

	int SetRealPos(long long llRealPos);

private:

	FILE *m_hFile;

	long long m_llFileSize;
	long long m_llSetPos;
	long long m_llRealPos;
	int m_nPosPart;

	char* m_pBuf;
	int m_nBufLen;
	char* m_pTempReadBuf;
	int m_nTempReadBufSize;
	int m_nReadSize;

	int m_nPageSize;
	long long m_llBufBeginPos;
	int m_nBufValidData;
	int m_nWriteBlockSize;

	int m_nReWriteBlockSize;

	long long m_llMaxByteForFlush;
};

#endif // !defined(AFX_XHEASYWRITER_H__ED2AC049_1830_41B9_9159_35B1B877C8CD__INCLUDED_)
