#ifndef xhEasyWriterApi_H
#define xhEasyWriterApi_H
 
#ifdef __cplusplus
extern "C" {
#endif

void* Api_New();
int Api_Open(void* writer,const char *pFileName);
int Api_GetFileHandler(void* writer);
int Api_SetWriteBlockSize(void* writer,int nWriteBlockSize);
int Api_Write(void* writer,char* pBuffer, int nLen);
int Api_Close(void* writer);
void Api_Delete(void* writer);

#ifdef __cplusplus
}
#endif
 
#endif